#Flatiron Exemplar to Portfolio Webpage
import os
import shutil
import subprocess
import re

flask_base_dir = '/Users/matthewmitchell/Documents/mmitchell_net/FlaskApp/'

def pwd():
	return run_cmd('pwd')

def run_cmd(cmd, cwd=None):
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True, cwd=cwd)
    (output, err) = p.communicate()
    printout = output.decode()
    return printout

def retrieve_git_url():
    #Get remote url
    cmd = "git remote -v"
    url = run_cmd(cmd)
    url = re.findall("origin\t(\S*).*", url)[0]
    return url

def find_root_images():
    printout = run_cmd("ls images/*")
    imgs = printout.split('\n')
    imgs = [img for img in imgs if img != '']
    return imgs

def get_lesson_name():
	return run_cmd('pwd').lower().strip().split('/')[-1].replace('dsc-','').replace('-','_')


def convert(solution=False, new=True):
	name = get_lesson_name()

	if solution:
		name += '_answers'
	#Get current working directory
	cwd = run_cmd('pwd').strip()
	#Convert to HTML
	run_cmd("jupyter nbconvert --to html --template basic index.ipynb")

	url = retrieve_git_url()
	title = name.replace("_"," ").title()
	#Modify HTML
	to_remove = [
	    """<script src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.1.10/require.min.js"></script>""",
	    """<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>"""
	]
	start = ["""{% extends "base.html" %}\n""", """{% block page_content %}\n""",
			"""\n\n""", """<!-- Include library -->""",
			"""<link rel="stylesheet" href="static/css/solarized-dark.css">\n""",
			"""<script src="static/js/highlight.pack.js"></script>\n""",
			"""<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>\n""",
			"""<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>\n""",
			"""\n\n""",
			"""<!-- Initialize highlight -->\n""",
			"""<script>hljs.initHighlightingOnLoad();</script>\n""",
			"""<!-- Start Intro Blurb -->\n""",
			"""<section class="jumbotron text-center">\n""",
			"""      <div class="container">\n""",
			"""        <h1 class="display-4">{}</h1>\n""".format(title),
			"""        <!--<p class="lead text-muted-4 mx-lg-5 px-lg-5 mt-lg-5 pt-lg-5"></p>-->\n""",
			"""        <br>\n""",
			"""        <div class="mb-1 text-muted" style="align-self: flex-end;">\n""",
			"""        <a href={}>\n""".format(url),
			"""        View source code on github: \n""",
			"""        <img src="static/images/github.png" width="50px"></a>\n""",
			"""        </div>\n""",
			"""      </div>\n""",
			"""  </section>\n""",
			"""  <!-- End Intro Blurb -->\n""",
			"""<div class="container" id="page-contents">\n""",
			"""\n""",
			"""  <div class="row">\n""",
			"""    <div class="col-md-10 blog-main">\n"""
			]
	end = [ """</div>\n""",
			"""</div>\n""",
			"""</div>\n""",
		    """{% endblock %}"""]
	with open('index.html') as f:
	    contents = f.readlines()
	    contents = start + contents + end
	    remove_idx = []
	    for i, line in enumerate(contents):
	        if line.strip() in to_remove:
	            remove_idx.append(i)
	        #Add Highlighting Ques
	        if '<div class=" highlight hl-ipython3">' in line:
	            contents[i] = line.replace("""<div class=" highlight hl-ipython3"><pre><span></span>""",
	                                       """<div class=" highlight hl-ipython3"><pre><code class="python">""")
	        if '</pre></div>' in line:
	            contents[i] = line.replace("""</pre></div>""",
	                                      """</pre></code></div>""")
	        if '&#182;' in line:
	        	contents[i] = line.replace("&#182;","")
	        if re.match('.*?[$].*?[$].*?', line):
	                 contents[i] = re.sub('(.*?)([$])(.*?)([$])(.*?)', "\g<1> \2\( \g<3> \4\) \g<5>", line)
	    for i, idx in enumerate(remove_idx):
	        trash = contents.pop(idx-i) #correct index shifts as previous idx removed
	    # <!-- Start Intro Blurb -->
	    # <section class="jumbotron text-center">
	    #     <div class="container">
	    #       <h1 class="display-4">Titles</h1>
	    #       <p class="lead text-muted-4 mx-lg-5 px-lg-5 mt-lg-5 pt-lg-5">
	    #        Text Text Text
	    #          </p>
	    #       <br>
	    #     </div>
	    # </section>
	    # <!-- End Intro Blurb -->

	if new==False:
		run_cmd("""rm /Users/matthewmitchell/Documents/mmitchell_net/FlaskApp/templates/{}.html""".format(name))

	#Update image path links
	f = open("index.html", "w")
	f.writelines(contents)
	f.close()    

	#Rename and Move
	shutil.move('index.html', '/Users/matthewmitchell/Documents/mmitchell_net/FlaskApp/templates/{}.html'.format(name))

	#Create Route/View Function in __init__.py
	if new:
		route_file = '/Users/matthewmitchell/Documents/mmitchell_net/FlaskApp/__init__.py'
		route_view = ["""\n""",
					  """@app.route('/{}')\n""".format(name),
		              """def {}():\n""".format(name),
		              """    return render_template('{}.html')\n""".format(name)
		             ]
		with open(route_file) as f:
		    contents = f.readlines()
		    split_idx = [idx for idx, line in enumerate(contents) if line == '#Resume\n'][0]
		    for route in route_view[::-1]:
		        contents.insert(split_idx, route)

		    #Dev Testing
		#     for line in contents[split_idx:split_idx+5]:
		#         print(line)
		f = open(route_file, "w")
		f.writelines(contents)
		f.close()

	#Create subfolder FlaskApp/static/images/
	#Move any images from folder/images to FlaskApp/static/images/subfolder
	if find_root_images() != []:
		shutil.copytree('{}/images/'.format(cwd), flask_base_dir+'static/images/{}'.format(name)+'/')

